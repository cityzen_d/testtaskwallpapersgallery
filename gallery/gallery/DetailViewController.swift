//
//  ViewController.swift
//  gallery
//
//  Created by Ivan on 4/1/19.
//  Copyright © 2019 Ivan. All rights reserved.
//

import UIKit


import AVFoundation
import AVKit


class DetailViewController: UIViewController {
    
    let playerVC = AVPlayerViewController()
    var avPlayer: AVPlayer?
    
    var imageUrl: String = ""
    var videoUrl: String = ""
    var imgL: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(videoUrl)
        let url = URL(string: videoUrl)
        
        if let movUrl = url {
            self.avPlayer = AVPlayer(url: movUrl)
            self.playerVC.player = self.avPlayer
            }
        
    }
    
        
        
        // Do any additional setup after loading the view, typically from a nib.
    @IBAction func playAction(_ sender: UIButton) {
        self.present(self.playerVC, animated: true){
        self.playerVC.player?.play()
        }
        
    }
    
}






//
//  Wallpaper.swift
//  gallery
//
//  Created by Ivan on 4/1/19.
//  Copyright © 2019 Ivan. All rights reserved.
//

import Foundation
import UIKit

class Wallpaper {
    var id: String
    var image_path: String
    var movie_path: String
    var image: UIImage?
    
    init(id: String, image_path: String, movie_path: String, image: UIImage){
        self.id = id
        self.image_path = image_path
        self.movie_path = movie_path
        self.image = image
    }
}


struct JSONWallaper: Decodable {
    struct JSONData: Decodable {
        var id: String
        var image_path: String
        var movie_path: String
        //the fields updated at and created at are not needed, so I did not init them
        
    }
    struct JSONLinks: Decodable {
        var first: String
        var last: String?
        var prev: String?
        var next: String
    }
    struct JSONMeta: Decodable {
        var current_page: Int
        var from: Int
        var last_page: Int
        var path: String
        var per_page: Int
        var to: Int
        var total: Int
    }
    var meta: JSONMeta
    var links: JSONLinks
    var data: [JSONData]
}

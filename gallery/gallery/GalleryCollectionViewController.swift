//
//  GalleryCollectionViewController.swift
//  gallery
//
//  Created by Ivan on 4/1/19.
//  Copyright © 2019 Ivan. All rights reserved.
//

import UIKit

private let reuseIdentifier = "cell"



var wallpapers = [Wallpaper]()

let str = "https://wallpapers.mediacube.games/api/photos"

class GalleryCollectionViewController: UICollectionViewController {
    @IBOutlet var galleryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        downloadData(url: str)
        galleryCollectionView.reloadData()
        
    }

    
    // MARK: - Navigation


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showLive"){
            let destinacionVC: DetailViewController = segue.destination as! DetailViewController
            if let indexPath = self.galleryCollectionView.indexPathsForSelectedItems{
                let selectedItem = wallpapers[indexPath[0].row]
                destinacionVC.imageUrl = selectedItem.image_path
                destinacionVC.videoUrl = selectedItem.movie_path
                
            }
        }
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return wallpapers.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WallpaperCollectionViewCell
        let wallpaper = wallpapers[indexPath.row]
        cell.wallpaperImage.image = wallpaper.image
        
    
        
    
        return cell
    }

    func downloadData(url: String){
        guard let url = URL(string: url) else {
            print("Invalid url")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        session.dataTask(with: request){ (data, response, error) in
            //catch error
            if let error = error {
                print(error)
                return
            }
            //cheking status code on normalin yo
            guard let res = response as? HTTPURLResponse, (200...299).contains(res.statusCode) else {
                print("bad response")
                return
            }
            guard let data = data else {
                print("Invalid data")
                return
            }
            //Decode our json and load in data source array
            do{
                let jsonLinks = try JSONDecoder().decode(JSONWallaper.self, from: data)
                print(jsonLinks)
                DispatchQueue.main.async {
                    do{
                        for node in jsonLinks.data {
                            let data = try Data(contentsOf: URL(string: node.image_path)!)
                            wallpapers.append(Wallpaper(id: node.id, image_path: node.image_path, movie_path: node.movie_path, image: UIImage(data: data)!))
                       // if(jsonLinks.meta.current_page < jsonLinks.meta.last_page){
                                //self.downloadData(url: jsonLinks.links.next)
                                
                         //   }
                        }
                        self.galleryCollectionView.reloadData()
                        
                    }catch{
                        print(error)
                        return
                    }
                }
                
            }catch{
                print(error)
                return
            }
            
            
        }.resume()
    }
}

    

    


